import React, {useEffect, useReducer} from 'react';
import {initialState, reducer} from "../../store/useReducer";
import {FETCH_TVSHOW_SUCCESS, fetchTvShowFailure} from "../../store/action";
import './SHow.css'
import axios from "axios";

const Show = ({match}) => {
    const [state, dispatch] = useReducer(reducer, initialState);

    useEffect(() =>{
        const fetchData = async() => {
            const response = await axios.get(`https://api.tvmaze.com/shows/${match.params.id}`);
            dispatch({type: FETCH_TVSHOW_SUCCESS, changes: response.data});
            console.log(state.tvShow)
        }
        fetchData().finally(e => dispatch(fetchTvShowFailure(e)))
    }, [match.params.id])




    return ( state.tvShow &&
        <div className='show'>
            <div className="image"><img src={state.tvShow.image.medium} alt="shows"/></div>
            <ul className="text">
                <li>Average Run time: {state.tvShow.averageRuntime}</li>
                <li>Genres: {state.tvShow.genres[0]} {state.tvShow.genres[1]}</li>
                <li>Language: {state.tvShow.language}</li>
                <li>Name: {state.tvShow.name}</li>
                <li>Network name: {state.tvShow.network.name}</li>
                <li>Premiered: {state.tvShow.premiered}</li>
                <li>Status: {state.tvShow.status}</li>
                <li>{state.tvShow.summary}</li>
                <li>Url: {state.tvShow.url}</li>
                <li>Type: {state.tvShow.type}</li>
            </ul>
        </div>
    );
};

export default Show;