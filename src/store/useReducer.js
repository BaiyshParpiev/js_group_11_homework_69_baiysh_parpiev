import {
    FETCH_TVSHOW_FAILURE,
    FETCH_TVSHOW_REQUEST,
    FETCH_TVSHOW_SUCCESS, FETCH_TVSHOWS_FAILURE,
    FETCH_TVSHOWS_REQUEST,
    FETCH_TVSHOWS_SUCCESS, ID
} from "./action";


export  const initialState = {
    tvShows: [],
    name: [],
    tvShow: '',
    id: '',
    loading: false,
    error: '',
};

export const reducer = (state, action) => {
    const addShow = (state, action) => {
        return {...state, tvShow: action.changes};
    }
    switch (action.type) {
        case ID:
            return {...state, id: action.payload}
        case FETCH_TVSHOW_REQUEST :
            return {...state, loading: true, error: null};
        case FETCH_TVSHOW_SUCCESS:
            return addShow(state, action);
        case FETCH_TVSHOW_FAILURE:
            return {...state, loading: false, error: action.payload};
        case FETCH_TVSHOWS_REQUEST :
            return {...state, loading: true, error: null};
        case FETCH_TVSHOWS_SUCCESS:
            return {...state, loading: false, tvShows: action.payload};
        case FETCH_TVSHOWS_FAILURE:
            return {...state, loading: false, error: action.payload};
        default:
            return state;
    }
};