export const FETCH_TVSHOW_REQUEST = 'FETCH_TVSHOW_REQUEST';
export const FETCH_TVSHOW_SUCCESS = 'FETCH_TVSHOW_SUCCESS';
export const FETCH_TVSHOW_FAILURE = 'FETCH_TVSHOW_FAILURE';

export const ID = 'ID';

export const FETCH_TVSHOWS_REQUEST = 'FETCH_TVSHOWS_REQUEST';
export const FETCH_TVSHOWS_SUCCESS = 'FETCH_TVSHOWS_SUCCESS';
export const FETCH_TVSHOWS_FAILURE = 'FETCH_TVSHOWS_FAILURE';

export const id = i => ({type: ID, payload: i});
export const fetchTvShowRequest = () => ({type: FETCH_TVSHOW_REQUEST});
export const fetchTvShowFailure = () => ({type: FETCH_TVSHOW_FAILURE});

export const fetchTvShowsRequest = () => ({type: FETCH_TVSHOWS_REQUEST});
export const fetchTvShowsSuccess = shows => ({type: FETCH_TVSHOWS_SUCCESS, payload : shows});
export const fetchTvShowsFailure = () => ({type: FETCH_TVSHOWS_FAILURE});



