import './App.css';
import TvShows from "./TvShows";
import {BrowserRouter, Route} from "react-router-dom";
import Show from "./Components/Shows/Show";


function App() {
  return (
      <BrowserRouter>
          <header style={{marginBottom: '40px', background: 'black', color: 'white', height: '70px', lineHeight: '70px', textAlign: 'center'}}>Tv shows</header>
          <TvShows/>
          <Route path='/shows/:id' exact component={Show}/>
      </BrowserRouter>
  );
}

export default App;
