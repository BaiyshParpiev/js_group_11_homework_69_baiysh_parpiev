import React, {useReducer} from 'react';
import {initialState, reducer} from "./store/useReducer";
import {Autocomplete} from "@material-ui/lab";
import TextField from '@material-ui/core/TextField';
import {fetchTvShowsFailure, fetchTvShowsSuccess} from "./store/action";
import axios from "axios";
import {useHistory} from "react-router-dom";

const TvShows = () => {
    const [state, dispatch] = useReducer(reducer, initialState);
    const history = useHistory();

    const onHandlerClick = (e) => {
        if(e.target.value.length < 3) return;
        const fetchData = async() => {
            const response = await axios.get(`https://api.tvmaze.com/search/shows?q=${e.target.value}`);
            if(response.data === null){
                dispatch(fetchTvShowsSuccess(''))
            }else{
                dispatch(fetchTvShowsSuccess(response.data))
            }
        }
        fetchData().finally(e => dispatch(fetchTvShowsFailure(e)))
    }


    return  ( state.tvShows &&
        <Autocomplete
            onChange={(event, newValue) => {
                history.replace('/shows/' + newValue.show.id)
            }}
            id="combo-box-demo"
            options={state.tvShows}
            getOptionLabel={(option) => option.show.name}
            style={{ width: 300 }}
            renderInput={(params) =>
                <TextField
                    onChange={e => onHandlerClick(e)}
                    {...params}
                    label="Search TV-Shows" variant="outlined" />}
        />
    )

};

export default TvShows;